

public class Main {
    public static void main(String[] args) throws Exception {

            int[] range = new InputRange().getRange();
            int min = range[0];
            int max = range[1];
            final TicketsStatistics s = new TicketsStatistics(min, max);
            s.perform();
            s.printResult();

    }
}




