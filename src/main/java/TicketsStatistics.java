public class TicketsStatistics {

    private int simple;
    private int complex;

    private final int min;
    private final int max;


    public TicketsStatistics(int min, int max) {
        this.min = min;
        this.max = max;
    }


   public void perform() throws Exception {
        final NumberParser  s = new SimpleStrategyNumberParser();
        final NumberParser  c = new ComplexStrategyNumberParser();

        this.simple=s.isHappyTicket(min,max);
        this.complex=c.isHappyTicket(min, max);


    }

    public void printResult () {
        System.out.println("Number of simple= " + this.simple + ", complex="+ this.complex);
        if(this.simple > this.complex)
            System.out.println("Simple strange won!");
        if(this.complex > this.simple)
            System.out.println("Complex strange won!");
        else
        System.out.println("This is no winnig strategy");
    }
}
