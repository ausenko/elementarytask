import java.util.InputMismatchException;
import java.util.Scanner;

public class InputRange {


    private boolean isValidNumber(int value) {
        int min = 000_001;
        int max = 999_999;
        return min <= value && value <= max;
    }


    private void printInstructionsForMin() {
        System.out.println("Invalid value. Value must be a number > 1");
    }


    private void printInstructionsForMax() {
        System.out.println("Invalid value. Value must be a number > 1 and number < 999_999");
    }


    private int getMinUserInput() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter min value: ");
        while (true) {
            try {
                int value = s.nextInt();
                if (this.isValidNumber(value)) return value;
            } catch (InputMismatchException e) {
                s.next();
            }
            this.printInstructionsForMin();
        }
    }


    private int getMaxUserInput(final int min) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter max value: ");


        while (true) {
            try {
                int value = s.nextInt();
                if (this.isValidNumber(value) && min < value) return value;
            } catch (InputMismatchException e) {
                s.next();
            }
            this.printInstructionsForMax();
        }
    }


    public int[] getRange() {

        int min = this.getMinUserInput();
        int max = this.getMaxUserInput(min);
        return new int[]{min, max};
    }


}
