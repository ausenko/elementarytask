package com.softserve.anton.task_2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EnvelopeUtil {

    private static final String YES_SRING_REGEX = "[yY][eE][sS]";
    private static final String Y_STRING_REGEX = "[yY]";


    public  static boolean checkYesValue(String value) {
        return value.matches(YES_SRING_REGEX)|| value.matches(Y_STRING_REGEX);
    }


    public static boolean checkToContinue() {
        Scanner scanner = new Scanner(System.in);
        try{
            System.out.println("Wold you like to continue?: ");
            String yes = scanner.nextLine();
            return EnvelopeUtil.checkYesValue(yes);
        }catch (InputMismatchException ex){
            System.out.println("Your parameters are incorrect");
            return false;
        }

    }


    public static void showEnableToPut(boolean enable) {
        if(enable) {
            System.out.println("Can't put");
        }else{
            System.out.println("Can put");
        }
    }
}
