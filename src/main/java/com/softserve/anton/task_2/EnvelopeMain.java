package com.softserve.anton.task_2;

public class EnvelopeMain {

    public static void main(String[] args) {
        EnvelopeFactory envelopeFactory = new EnvelopeFactory();

        do {
            Envelope firstEnvelope = envelopeFactory.getEnvelope();
            Envelope secondEnvelope = envelopeFactory.getEnvelope();
            if ((firstEnvelope != null) && (secondEnvelope != null)) {
                EnvelopeUtil.showEnableToPut(firstEnvelope.isEnableToPut(secondEnvelope));
            }

        }
        while (EnvelopeUtil.checkToContinue());
    }
}
