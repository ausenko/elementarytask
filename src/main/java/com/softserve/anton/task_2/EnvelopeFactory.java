package com.softserve.anton.task_2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EnvelopeFactory {
    public Envelope getEnvelope() {
        Scanner scanner = new Scanner(System.in);
        try{
            System.out.println("Enter width: ");
            double width = scanner.nextDouble();
            System.out.println("Enter height: ");
            double height = scanner.nextDouble();
            return new Envelope(width, height);
        }catch (InputMismatchException ex) {
            System.out.println("Your parameters are incorrect");
            return null;
        }
    }
}
