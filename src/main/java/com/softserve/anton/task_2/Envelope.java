package com.softserve.anton.task_2;

public class Envelope {
    private double width;
    private double height;


    public Envelope() {

    }


    public Envelope(double width, double height) {
        this.width = width;
        this.height = height;

    }


    public boolean isEnableToPut(Envelope envelope) {
        return (width < envelope.width) && (height < envelope.height);
    }
}
