package com.softserve.anton.task_3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class YesValidator {

    private static final String YES_STRING_REGEX = "[yY][eE][sS]";
    private static final String Y_STRING_REGEX = "[yY]";

    private static boolean checkYesValue(String value) {
        return value.matches(YES_STRING_REGEX) || value.matches(Y_STRING_REGEX);
    }

    public static boolean checkToContinue() {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Would u like to continue?: ");
            String yesPattern = in.nextLine();
            return checkYesValue(yesPattern);
        } catch (InputMismatchException ex) {
            System.out.println("Your parameters are incorrect");
            return false;
        }
    }

}

