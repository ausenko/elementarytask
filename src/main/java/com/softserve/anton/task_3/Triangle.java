package com.softserve.anton.task_3;

public class Triangle implements Comparable<Triangle> {

    private String name;
    private double sides[];

    public Triangle(){
        this.name = null;
        this.sides = new double[3];
    }

    public Triangle(String name, double[] sides) {
        this.name = name;
        this.sides = sides;
    }

    public double calculateArea() {
        double p = (sides[0] + sides[1] + sides[2]) / 2;
        return Math.sqrt(p * (p - sides[0]) * (p - sides[1]) * (p - sides[2]));
    }

    public String getName() {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public double[] getSides() {
        return sides;
    }

    public void setSize(double[] sides) {
        this.sides = sides;
    }


    public int compareTo(Triangle o) {
        return -Double.compare(calculateArea(), o.calculateArea());
    }
}
