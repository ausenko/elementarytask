package com.softserve.anton.task_3;

import java.util.Scanner;
import java.util.TreeSet;

public class TriangleMain {

    public static void main(String[] args) {
        TriangleFactory factory = new TriangleFactory();
        TreeSet<Triangle> triangles = new TreeSet<Triangle>();
        do {
            System.out.println("Enter ur triangle: ");
            Scanner in = new Scanner(System.in);
            String value = in.nextLine();
            try {
                Triangle triangle = factory.getTriangle(value);
                if (triangle != null) {
                    triangles.add(factory.getTriangle(value));
                }
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }
        } while (YesValidator.checkToContinue());
        for (Triangle temp : triangles) {
            System.out.println("[" + temp.getName() + "]: " + temp.calculateArea());
        }
    }
}
