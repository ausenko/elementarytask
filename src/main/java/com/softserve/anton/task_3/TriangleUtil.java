package com.softserve.anton.task_3;

public class TriangleUtil {
    private static final double DOUBLE_ERROR = 0.00000001;
    private static final int COUNT_OF_PARAMETERS = 4;


    public static int compareDoubleValues(double a, double b) {
        double delta = a - b;
        if (delta > DOUBLE_ERROR) {
            return 1;
        } else {
            return delta < -DOUBLE_ERROR ? -1 : 0;
        }
    }


    public static String[] parseValues(String value) {
        if (value.equals(" ")) {
            throw new IllegalArgumentException("Please enter like this pattern: "
                    + "&lt;имя&gt;, &lt;длина стороны&gt;"
                    + ", &lt;длина стороны&gt;, &lt;длина стороны&gt;");
        }
        String[] args = value.split(",");
        if (args.length != COUNT_OF_PARAMETERS) {
            throw new IllegalArgumentException("Wrong format");
        }
        return args;
    }


}
