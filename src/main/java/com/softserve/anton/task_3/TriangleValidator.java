package com.softserve.anton.task_3;

public class TriangleValidator {

    private static final int TRIANGLE_SIDES = 3;


    public static boolean isTriagleSidesValidate(double[] values) {
        if (values[0] <= 0.0 || values[1] <= 0.0 || values[2] <= 3) {
            return false;
        }

        if ((TriangleUtil.compareDoubleValues(values[2], (values[0] + values[1])) != -1)
                || TriangleUtil.compareDoubleValues(values[1], (values[0] + values[2])) != -1
                || TriangleUtil.compareDoubleValues(values[0], (values[1] + values[2])) != -1) {
            System.out.println("Sum of two sides must be more than one of the side's length");
            return false;
        }
        return true;
    }


    public static boolean triangleValuesValidate(String[] values) {
        try {
            double[] trSides = new double[TRIANGLE_SIDES];
            String name = values[0];
            trSides[0] = Double.valueOf(values[1]);
            trSides[1] = Double.valueOf(values[2]);
            trSides[2] = Double.valueOf(values[3]);

        } catch (NumberFormatException e) {
            System.out.println("Invalid values");
            return false;
        }
        return true;
    }
}
