package com.softserve.anton.task_3;

public class TriangleFactory {

    private static final int TRIAGLE_SIDES = 3;

    public Triangle getTriangle(String value) {
        String[] values = TriangleUtil.parseValues(value);
        if (TriangleValidator.triangleValuesValidate(values)){
            double[] triagleSides = new double[TRIAGLE_SIDES];
            triagleSides[0] = Double.valueOf(values[1]);
            triagleSides[1] = Double.valueOf(values[2]);
            triagleSides[2] = Double.valueOf(values[3]);
            if (TriangleValidator.isTriagleSidesValidate(triagleSides)) {
                return new Triangle(values[0].replace("\\s", " "), triagleSides);
            }
        }
        return null;
    }
}
