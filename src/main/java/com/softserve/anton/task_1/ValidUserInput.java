package com.softserve.anton.task_1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ValidUserInput {

    private boolean isValidUserInputValue(int value) {
       return value > 0;
    }

    private void printInstructionsForValue() {
        System.out.println("Invalid value. Value must be a number > 1");
    }


    public int getValueUserInput() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter  value: ");
        while (true) {
            try {
                int value = s.nextInt();
                if (this.isValidUserInputValue(value)) return value;
            } catch (InputMismatchException e) {
                s.next();
            }
            this.printInstructionsForValue();
        }
    }

}
