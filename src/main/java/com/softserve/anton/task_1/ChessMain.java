package com.softserve.anton.task_1;


public class ChessMain {

    public static void main(String[] args) {

        ValidUserInput value = new ValidUserInput();
        int width = value.getValueUserInput();
        int height = value.getValueUserInput();

        ChessBoard board = new ChessBoard(width, height);
        board.print();
    }
}
