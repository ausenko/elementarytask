package com.softserve.anton.task_1;



class ChessBoard {


    private static final String WHITE_SQUARE = "\u25A1";
    private static final String BLACK_SQUARE = "\u25A0";

    private int width;
    private int height;

    public ChessBoard() {

    }

    public ChessBoard(int width, int height) {
        this.width = width;
        this.height = height;
    }


    public void print() {

        for (int i = 0; i < height; i++) {
            final boolean even = i % 2 == 0;

            for (int j = 0; j < width; j++) {
                if (even)
                    System.out.print(BLACK_SQUARE + WHITE_SQUARE);
                else
                    System.out.print(WHITE_SQUARE + BLACK_SQUARE);
            }
            System.out.println();
        }
    }
}


