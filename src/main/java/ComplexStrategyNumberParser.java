

public class ComplexStrategyNumberParser implements NumberParser {
    private static final int COUNT_OF_DIGITS = 6;
    private static int CountHappyTickets = 0;


    public int isHappyTicket(int min, int max) {

        for (int i = min; i <= max; i++) {
            int tempValue = i;
            int firstSum = 0;
            int secondSum = 0;
            for (int j = 0; j < COUNT_OF_DIGITS; j++) {
                if (j % 2 == 0) {
                    firstSum += tempValue % 10;
                    tempValue /= 10;
                } else {
                    secondSum += tempValue % 10;
                    tempValue /= 10;
                }
            }
            if (secondSum == firstSum) {
                CountHappyTickets++;
            }
        }
        return CountHappyTickets;

    }
}
