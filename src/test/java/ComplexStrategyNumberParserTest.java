import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComplexStrategyNumberParserTest {

    private ComplexStrategyNumberParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new ComplexStrategyNumberParser();
    }

    @After
    public void tearDown() throws Exception {
        parser = null;
    }

    @Test
    public void isHappyTicket() {
        //GIVEN
        int expected = 55252;
        //WHEN
        int actual = parser.isHappyTicket(0,999999);
        //THEN
        assertEquals(expected,actual);
    }
}